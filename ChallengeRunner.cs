﻿namespace ConsoleAppTest
{
    internal class ChallengeRunner
    {
        public void RunChallenge(Challenge challenge)
        {
            challenge.In();
            challenge.Run();
            challenge.Out();
        }

    }
}