﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest
{
    interface Challenge
    {
        void In();
        void Run();
        void Out();
    }
}
