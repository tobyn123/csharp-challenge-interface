﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest
{
    class FizzBuzzChallenge : Challenge
    {
        public void In()
        {
            Console.WriteLine("FizzBuzz challenge is about to begin");
        }

        public void Out()
        {
            Console.WriteLine("FizzBuzz challenge is about to begin");
        }

        public void Run()
        {
            int numbers_to_fizz = 100;
            int fizz = 3, buzz = 5;
            for (int i = 1; i < numbers_to_fizz + 1; i++)
            {
                if (i % fizz == 0 && i % buzz == 0)
                    Console.WriteLine("FIZZBUZZ");
            else if (i % fizz == 0)
                    Console.WriteLine("fizz");
            else if (i % buzz == 0)
                    Console.WriteLine("buzz");
            else
                    Console.WriteLine(i);
            }
        }
    }
}
